/* This function calculates the pseudo-spectrum of the PR-UCF estimator. The idea is
 * to perform a bisection search on the derivative of the objective function with respect
 * to \rho (or in the paper \sigma_s^2). The method to determine the eigenvalues is based 
 * on the paper "Solving Secular Function Stably and Efficiently" using the
 * Middle Way method.
 * Input:
 *        d: a row vector of size 1 x NAnt: the initial eigenvalues without
 *           any perturbation, which is also the elements on the diagonal of
 *           the matrix D. Elements of d must be sorted ascendingly
 *        rho: a row vector of size 1 x NGrid: the scaling factor. Must be real
 *        absz: a row vector of size 1 x (NAnt*NGrid): contains the absolute
 *               value of the vector z for each grid point. For example,
 *               the first NAnt-values of absz are the absolute value
 *               of the first gridpoint and so on...
 *        NSrc: the number of source signals (or the number of required eigenvalues plus one).
 *        NAnt: the size of the matrix D
 *        NGrid: the number of gridpoints (steering vectors) for tracking
 *		  bf_spec: a row vector of size 1 x NGrid: the beamformer spectrum
 *Output:
 *			numIt: a row vector of size 1 x NGrid: returns the number of bisection steps
 *				   for each gridpoint
 *			optimal_rho: a row vector of size 1 x NGrid: returns the optimal value of
 *						 rho (or in the paper \sigma_s^2) for each grid point
 *          objVal: a row vector of size 1 x NGrid: returns the optimal value of
 *					the PR-UCF optimization problem for each grid point
 */

#include "mex.h"
#include "matrix.h"
#include "math.h"
#include <mkl_lapack.h>
#include <mkl_cblas.h>
#include <mkl.h>
#include <mkl_types.h>
#include <mkl_service.h>
#include <omp.h>


/* This subfunction calculates the derivative of the objective function with respect to the scalar \rho (or equivalently \sigma_s^2). For more
 * information, please refer to Equation (65) in the paper
 * Input:
 *        bf_spec_perGrid: the conventional beamformer spectrum at the given gridpoint
 *        rho: the scalar at which the derivative is calculated
 *        eigVal: a row vector of size 1 x (NSrc-1): the eigenvalues of the modified Hermitian matrix
 *				  with the scalar \rho
 *		  absz_perGrid: a row vector of size 1 x NAnt, containing the absolute valued of each entries in vector z in the paper
						 See Equation (65)
 *        NAnt: the size of the matrix D
 *        NSrc: the number of source signals (or the number of required eigenvalues plus one).
 *Output:
 *			derivVal: the first entry contains the derivative of the objective function at the point rho
*/
void calcDeriv(double bf_spec_perGrid, double rho, double *eigVal, double *absz_perGrid, double *delta_mat, MKL_INT NAnt, MKL_INT NSrc, double *derivVal) {
	double num, den;
	MKL_INT iSrc, iAnt;
	derivVal[0] = 0;
	for (iSrc = 0; iSrc < (NSrc - 1); iSrc++) {
		num = -2 * eigVal[iSrc];
		den = 0;
		for (iAnt = 0; iAnt < NAnt; iAnt++) {
			den += (absz_perGrid[iAnt]*absz_perGrid[iAnt] / (delta_mat[iSrc*NAnt + iAnt] * delta_mat[iSrc*NAnt + iAnt]));
		}
		derivVal[0] += (num / (rho*rho*den));
	}
	derivVal[0] += (2 * rho - 2 * bf_spec_perGrid/NAnt);
}

/*This subfunction returns the eigenvalues of the modified Hermitian matrix.
* Basically it is a wrap function for the main dlaed4() routine of LAPACK.
* Input:
*        d: a row vector of size 1 x NAnt: the initial eigenvalues without
*           any perturbation, which is also the elements on the diagonal of
*           the matrix D. The elements of d must be sorted ascendingly
*        rho: the scaling factor of the rank-one term. Must be scalar and positive
*        absz_perGrid: a row vector of size 1 x NAnt: contains the absolute
*               value of the vector z for one grid point.
*        NAnt: the size of the matrix D
*        NSrc: the number of source signals (all the eigenvalues from the first to the (NSrc-1) will be calculated)
*
*Output:
*        eig: a row vector of size 1 x (NSrc-1): returns the calculated
*                eigenvalues for each grid point. The eigenvalues are sorted
*				 in ascending order.
*       delta_mat: a row vector containing the difference between the modified
*                  eigenvalues and the poles in the rational function
 *      info: output message of the dlaed4 routine
*/
void calcEig(double *d, double rho, double *absz_perGrid, MKL_INT NAnt, MKL_INT NSrc, double *eig, double* delta_mat, MKL_INT info) {
	for (MKL_INT iEig = 1; iEig <= NSrc - 1; iEig++) {
		dlaed4(&NAnt, &iEig, d, absz_perGrid, delta_mat + ((iEig - 1)*NAnt), &rho, eig + (iEig - 1), &info);
	}
}


/* Main Function to MATLAB */
void mexFunction( int nlhs, mxArray *plhs[],
        int nrhs, const mxArray *prhs[])
{
    /* input variables*/
    double *d;
    double *absz;
    MKL_INT NAnt;
	MKL_INT NSrc;
	MKL_INT NGrid;
	double *bf_spec;
	/* output matrix */
    double *numIt;             
	double *optimal_rho;
	double *objVal;

	/* Variables */
	MKL_INT iGrid;
    

    /* Initialize values for input and output variables*/
    d = mxGetPr(prhs[0]);
    absz = mxGetPr(prhs[1]);
    NSrc = (MKL_INT)mxGetScalar(prhs[2]);
    NAnt = (MKL_INT)mxGetScalar(prhs[3]);
    NGrid = (MKL_INT)mxGetScalar(prhs[4]);
	bf_spec = mxGetPr(prhs[5]);

    plhs[0] = mxCreateDoubleMatrix(1, NGrid, mxREAL);
	plhs[1] = mxCreateDoubleMatrix(1, NGrid, mxREAL);
	plhs[2] = mxCreateDoubleMatrix(1, NGrid, mxREAL);
    numIt = mxGetPr(plhs[0]);
	optimal_rho = mxGetPr(plhs[1]);
	objVal = mxGetPr(plhs[2]);
    
  	
    /* Calculate the sum of the initial eigenvalues squared (corresponding to tr(R^2))*/
	
	MKL_INT incx = 1;
    double sumd2 = cblas_dnrm2(NAnt, d, incx);
	sumd2 = sumd2 * sumd2;

#pragma omp parallel
	{
		double bf_spec_perGrid;
		double *absz_perGrid, *swapPtr;
		double rho_left, rho_right, rho_mid;
		double optimal_rho_perGrid = -1.0;
		MKL_INT info, it1, it2;
		double *delta_mat = (double*) mkl_malloc((NAnt*(NSrc-1))*sizeof(double), 64);
		double *eig_left = (double*)mkl_malloc((NSrc - 1) * sizeof(double), 64);
		double *eig_right = (double*)mkl_malloc((NSrc - 1) * sizeof(double), 64);
		double *eig_mid = (double*)mkl_malloc((NSrc - 1) * sizeof(double), 64);
		double *tempDeriv = (double*)mkl_malloc(1 * sizeof(double), 64);
		double deriv_left, deriv_right, deriv_mid;
		#pragma omp for
		/*Main Routine*/
		for (iGrid = 0; iGrid < NGrid; iGrid++) {
			bf_spec_perGrid = bf_spec[iGrid];
			absz_perGrid = absz + iGrid*NAnt;
			
			/* Find the interval on which the derivative of the objective function changes sign from negative to positive*/
			/*Check if we can still use the previous rho. If yes then use it*/
			if (optimal_rho_perGrid < 0) {
				rho_left = 1e-6;
			}
			else {
				rho_left = optimal_rho_perGrid;
			}
			
            // Compute the modified eigenvalues
			calcEig(d, rho_left, absz_perGrid, NAnt, NSrc, eig_left, delta_mat, info);
            
            // Compute the derivative of the cost function
			calcDeriv(bf_spec_perGrid, rho_left, eig_left, absz_perGrid, delta_mat, NAnt, NSrc, tempDeriv);
			
			deriv_left = tempDeriv[0];
			if (deriv_left < 0) { /*If the derivative on the left point is negative, then keep doubling the right point and reassigning left and right points until the derivative on the right is positive*/
				it1 = 0;
				while (true) {
					it1 += 1;
					if (rho_left == 0) {
						rho_right = 1e-6;
					}
					else 
					{
						rho_right = 2 * rho_left;
					}
					calcEig(d, rho_right, absz_perGrid, NAnt, NSrc, eig_right, delta_mat, info);
					calcDeriv(bf_spec_perGrid, rho_right, eig_right, absz_perGrid, delta_mat, NAnt, NSrc, tempDeriv);
                    
					deriv_right = tempDeriv[0];
					if (fabs(deriv_right) < 1e-6) {
						optimal_rho[iGrid] = rho_right;
						break;
					}
					if (deriv_right < 0) {
						rho_left = rho_right;
						swapPtr = eig_left; eig_left = eig_right; eig_right = swapPtr;
					}
					else {
						break;
					}
				}
			}
			else {/*If the derivative on the left point is positive, assign this to the right point, keep dividing by two on the left point and reassigning until the derivative on the left point is negative */
				it1 = 0;
				rho_right = rho_left;
				swapPtr = eig_right; eig_right = eig_left; eig_left = swapPtr;
				while (true) {
					it1 += 1;
					rho_left = rho_right / 2;
					calcEig(d, rho_left, absz_perGrid, NAnt, NSrc, eig_left, delta_mat, info);
					calcDeriv(bf_spec_perGrid, rho_left, eig_left, absz_perGrid, delta_mat, NAnt, NSrc, tempDeriv);
					deriv_left = tempDeriv[0];
					if (fabs(deriv_left) < 1e-6) {
						optimal_rho[iGrid] = rho_left;
						break;
					}
					if (deriv_left > 0) {
						rho_right = rho_left;
						swapPtr = eig_right; eig_right = eig_left; eig_left = swapPtr;
					}
					else {
						break;
					}
				}
			}

			it2 = 0;
			if (optimal_rho[iGrid] == 0) {
				/* Perform bisection search between left and right point*/
				while ((rho_right - rho_left) > 1e-6) {
					it2 += 1;
					rho_mid = (rho_left + rho_right) / 2;
					calcEig(d, rho_mid, absz_perGrid, NAnt, NSrc, eig_mid, delta_mat, info);
					calcDeriv(bf_spec_perGrid, rho_mid, eig_mid, absz_perGrid, delta_mat, NAnt, NSrc, tempDeriv);
					deriv_mid = tempDeriv[0];
					if (fabs(deriv_mid) < 1e-6) {
						break;
					}
					if (deriv_mid > 0) {
						rho_right = rho_mid;
						swapPtr = eig_right; eig_right = eig_mid; eig_mid = swapPtr;
					}
					else {
						rho_left = rho_mid;
						swapPtr = eig_left; eig_left = eig_mid; eig_mid = swapPtr;
					}
				}
				optimal_rho[iGrid] = (rho_left + rho_right) / 2;
			}
			optimal_rho_perGrid = optimal_rho[iGrid];
			/*Recalculate the required eigenvalues at the optimal \rho*/
			calcEig(d, optimal_rho[iGrid], absz_perGrid, NAnt, NSrc, eig_mid, delta_mat, info);
			/*Assign the optimal value of the objective function and the total number of iterations where the eigenvalues computation is required*/
			double sumEig2 = cblas_dnrm2(NSrc - 1, eig_mid, incx);
			sumEig2 = sumEig2*sumEig2;
			objVal[iGrid] -= sumEig2;
			objVal[iGrid] = objVal[iGrid] + sumd2 - 2 * bf_spec_perGrid*optimal_rho[iGrid]/NAnt + optimal_rho[iGrid] * optimal_rho[iGrid];
			numIt[iGrid] = it1 + it2;

		}
		/*Free the memory of the matrices*/
		mkl_free(delta_mat);
		mkl_free(eig_left);
		mkl_free(eig_right);
		mkl_free(eig_mid);
		mkl_free(tempDeriv);
	}
 
}
