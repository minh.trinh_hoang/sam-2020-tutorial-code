function [ DOA_est, Spec_est, numIt] = prucf_bisection( Y, NSrc, Grid, AGrid, varargin )
%PRUCF_BISECTION returns the estimated DOA from the received signal and the
%estimated spectrum if requested using the proposed framework of Mats
%Viberg of the unconstrained covariance fitting where the estimated power
%from each direction is determined by bisection. The method of determining 
%the eigenvalues is using rational approximation. For more information, 
%please refer to "Solving Secular Function Stably and Efficiently"
%
%Input:   Y: Received signal (NAnt x NSnp)
%         NSrc: Number of sources
%         Grid: the sampled angle of view (1 x NGrid)
%         AGrid: the dictionary steering matrix corresponding to the
%         sampled angle of view (NAnt x NGrid)
%         Ryy: the covariance matrix of the received signal (optional)
%
% Output: DOA_est: the estimated DOA on the sampled angle of view (1 x NSrc)
%         Spec_est: the normalized pseudo-spectrum on the grid,  (1 x NGrid)
%                   where the highest peak has the unit height


% Initialize parameters
[NAnt, NSnp] = size(Y);
NGrid = length(Grid);
if isempty(varargin)
    R_est = 1/NSnp*(Y*Y');
else
    R_est = varargin{1};
end

% Perform full eigenvalue decomposition and diagonalization
[U, Lambda_hat] = eig(R_est);
lambda_hat = diag(real(Lambda_hat));
[lambda_hat, order] = sort(lambda_hat, 'descend');
U = U(:, order);

% Calculate the absolute value squared of each entry in the z vector for
% each angle of view
% ATilde = U'*AGrid;
% absZ = abs(ATilde);
% 
% % Calculate the Conventional Beamformer
% abs_ATilde_squared = abs(ATilde).^2;
% bf_spec = sum(bsxfun(@times, abs_ATilde_squared, lambda_hat), 1);
absZ = abs(U'*AGrid);
bf_spec = sum((absZ.*sqrt(lambda_hat)).^2, 1);

% Initialize the value of \sigma_s^2 for bisection
% if (lambda_hat(end)< 1e-9)
%     rho = 1e-5*ones(1, NGrid);
% else
%     rho = 1./sum(bsxfun(@times, abs_ATilde_squared, 1./lambda_hat), 1);
% end
% Calculate the optimal value of PR-UCF estimator for each grid point
[numIt, optimal_rho, Spec_est] = calcSpecUCF_bisection(-lambda_hat, absZ/sqrt(NAnt), NSrc, NAnt, NGrid, bf_spec);
% Calculate and normalize the pseudo-spectrum
Spec_est = 1./(Spec_est + 1e-6);
Spec_est = Spec_est./max(abs(Spec_est));

% Estimate the DOA based on the pseudo-spectrum
[~, locs] = findpeaks(Spec_est, 'SortStr', 'descend');
if length(locs)> NSrc
    DOA_est = Grid(locs(1:NSrc));
else
    DOA_est = [Grid(locs), 90*ones(1, NSrc - length(locs))];
end
DOA_est = sort(DOA_est(:));

end