The PR methods, which were implemented in *.c, can be called from the main 
script "main.m". The PR methods use BLAS and LAPACK functions from the Intel
MKL together with OMP for parallelization. If necessary, please follow the 
instruction from the Intel Development Website for more information to link
the code with the Intel MKL Library. The available code is only supported in
Linux and Windows